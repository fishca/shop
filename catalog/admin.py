# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import *

class ProduktImagesInline(admin.StackedInline):
    model = ProduktImages
    extra = 1

class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'slug', 'published', 'ordering', 'pic')
    list_editable = ('slug', 'published', 'ordering')
    
    
class ProduktAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'slug', 'published', 'ordering', 'pic')
    list_editable = ('slug', 'published', 'ordering')
    inlines = [ProduktImagesInline,]
    list_filter = ('category',)
    search_fields = ['name',]

admin.site.register(Category, CategoryAdmin)
admin.site.register(Produkt, ProduktAdmin)
admin.site.register(ProduktImages)



