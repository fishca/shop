# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Category'
        db.create_table(u'catalog_category', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=150, blank=True)),
            ('parent', self.gf('mptt.fields.TreeForeignKey')(blank=True, related_name='children', null=True, to=orm['catalog.Category'])),
            ('title', self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True)),
            ('meta_desc', self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True)),
            ('meta_key', self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True)),
            ('slug', self.gf('django.db.models.fields.CharField')(default='', max_length=250, blank=True)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(default='', max_length=100, blank=True)),
            ('published', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('ordering', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('lft', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('rght', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('tree_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('level', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
        ))
        db.send_create_signal(u'catalog', ['Category'])

        # Adding model 'Produkt'
        db.create_table(u'catalog_produkt', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=150, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True)),
            ('meta_desc', self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True)),
            ('meta_key', self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True)),
            ('slug', self.gf('django.db.models.fields.CharField')(default='', max_length=250, blank=True)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(default='', max_length=100, blank=True)),
            ('short_text', self.gf('tinymce.models.HTMLField')(blank=True)),
            ('full_text', self.gf('tinymce.models.HTMLField')(blank=True)),
            ('price', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=5, decimal_places=2, blank=True)),
            ('published', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('ordering', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
        ))
        db.send_create_signal(u'catalog', ['Produkt'])

        # Adding M2M table for field category on 'Produkt'
        m2m_table_name = db.shorten_name(u'catalog_produkt_category')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('produkt', models.ForeignKey(orm[u'catalog.produkt'], null=False)),
            ('category', models.ForeignKey(orm[u'catalog.category'], null=False))
        ))
        db.create_unique(m2m_table_name, ['produkt_id', 'category_id'])

        # Adding model 'ProduktImages'
        db.create_table(u'catalog_produktimages', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('produkt', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Produkt'], null=True, blank=True)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(default='', max_length=100, blank=True)),
        ))
        db.send_create_signal(u'catalog', ['ProduktImages'])


    def backwards(self, orm):
        # Deleting model 'Category'
        db.delete_table(u'catalog_category')

        # Deleting model 'Produkt'
        db.delete_table(u'catalog_produkt')

        # Removing M2M table for field category on 'Produkt'
        db.delete_table(db.shorten_name(u'catalog_produkt_category'))

        # Deleting model 'ProduktImages'
        db.delete_table(u'catalog_produktimages')


    models = {
        u'catalog.category': {
            'Meta': {'object_name': 'Category'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'meta_desc': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'meta_key': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '150', 'blank': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['catalog.Category']"}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'slug': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        u'catalog.produkt': {
            'Meta': {'object_name': 'Produkt'},
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'cat'", 'symmetrical': 'False', 'to': u"orm['catalog.Category']"}),
            'full_text': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'meta_desc': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'meta_key': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '150', 'blank': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '5', 'decimal_places': '2', 'blank': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'short_text': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'slug': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'})
        },
        u'catalog.produktimages': {
            'Meta': {'object_name': 'ProduktImages'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'produkt': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Produkt']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['catalog']